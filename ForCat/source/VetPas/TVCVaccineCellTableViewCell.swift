//
//  TVCVaccineCellTableViewCell.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 05/09/2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit

class TVCVaccineCellTableViewCell: UITableViewCell {

    @IBOutlet weak var lVaccineName: UILabel!
    @IBOutlet weak var lVaccineOn: UILabel!
    @IBOutlet weak var lVaccineOff: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
