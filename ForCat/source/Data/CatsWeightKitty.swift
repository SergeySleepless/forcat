//
//  CatsBreedsWeight.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 14.08.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import Foundation

let catsWeightKitty:[(minCritical: Float, minAverage: Float, maxAverage: Float, maxCritical: Float)] = [
    (0.3, 0.6, 0.1, 0.2),
    (0.3, 0.5, 0.8, 1.0),
    (0.7, 1.0, 1.5, 1.8),
    (1.3, 1.7, 2.3, 2.7),
    (2.1, 2.5, 3.6, 3.9),
    (2.9, 3.1, 4.2, 4.5),
    (3.1, 3.5, 4.8, 5.1)]
