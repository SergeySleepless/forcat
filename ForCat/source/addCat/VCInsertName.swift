//
//  insertName.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 24.07.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit

class VCInsertName: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var tfInsertName: UITextField!
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tfInsertName.autocapitalizationType = .sentences
        tfInsertName.delegate = self
        tfInsertName.inputAccessoryView = createToolBar()
        
        hideKeyboard()
    }
    
    // Передача данных в другое окно
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if self.tfInsertName.text != "" {
            if segue.identifier == "fromName" {
                if let catName = tfInsertName.text {
                    let dvc = segue.destination as! VCinsertPhoto
                    dvc.name = catName
                }
            }
        }
    }
    
    // Убирает клавиатуру по кнопке Return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.tfInsertName.text != "" {
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    
    func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                       target: self,
                                       action: nil)
        
        let doneButton = UIBarButtonItem(title: "Далее",
                                         style: .plain,
                                         target: self,
                                         action: #selector(VCInsertName.dismissKeyboard))
        
        
        toolBar.setItems([flexible, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    // Переопределяем segue
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        if let ident = identifier {
            if ident == "fromInsertName" {
                if tfInsertName.text == "" {
                    errorAlert()
                    return false
                }
            }
        }
        return true
    }
    
    // Вызывает ошибку
    func errorAlert() {
        let alert = UIAlertController(title: "Ошибка!", message: "Введите имя питомца.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
}

//=================--------------------------------------

extension VCInsertName {
    // Прячет клавиатуру при нажатии на View
    func hideKeyboard() {
        if self.tfInsertName.text != "" {
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(
                target: self,
                action: #selector(dismissKeyboard))
            
            view.addGestureRecognizer(tap)
        }
    }
    
    @objc func dismissKeyboard() {
        if tfInsertName.text != "" {
            view.endEditing(true)
        } else {
            errorAlert()
        }
    }
}
