//
//  VCEat.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 29.07.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

func prepare(feed: [Feed]) -> [Feed]{
    var newFeed = feed
    for item in feed {
        newFeed[Int(item.petIndex)] = item
    }
    return newFeed
}

func index(feed: [Feed], ind: Int) -> [Feed] {
    var newFeed: [Feed] = []
    for item in feed {
        if item.petIndex == ind {
            newFeed.append(item)
        }
    }
    return newFeed
}

class VCFeed: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tvFeed: UITableView!
    var feed: [Feed] = []
    @IBOutlet weak var navBar: UINavigationBar!
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {}
    @IBAction func swNotificationValueChanged(_ sender: UISwitch) {
        
        if !sender.isOn {
            for item in ForCat.index(feed: feed, ind: petIndex) {
                center.removePendingNotificationRequests(withIdentifiers: ["\(petIndex)\(item.time!)"])
            }
        }
        let userDefaults = UserDefaults.standard
        userDefaults.set(sender.isOn, forKey: "notification")
        userDefaults.synchronize()
        
        tvFeed.reloadData()
    }
    
    let center = UNUserNotificationCenter.current()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    lazy var context = appDelegate.persistentContainer.viewContext
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var feedBuf: [Feed] = []
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<Feed> = Feed.fetchRequest()
        do {
            feedBuf = try context.fetch(fetchRequest)
        } catch {
        }
        feed = ForCat.index(feed: feedBuf, ind: petIndex)
        if feed.count > 1 {
            feed.sort(by: { $0.time!.compare($1.time!) == .orderedAscending })
        }
        if ForCat.index(feed: feed, ind: petIndex).count > 0 {
            return ForCat.index(feed: feed, ind: petIndex).count + 2
        } else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if ForCat.index(feed: feed, ind: petIndex).count == 0 && indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NooneCell")
            return cell!
        }
        if indexPath.row < ForCat.index(feed: feed, ind: petIndex).count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellFeed") as! TVCFeed
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            dateFormatter.locale = Locale.init(identifier: "ru")
            cell.lTime.text = dateFormatter.string(from: feed[indexPath.row].time!)
            cell.lAbout.text = feed[indexPath.row].count! + " " + feed[indexPath.row].feed!
            cell.accessoryType = .none
            if Float(Date.init().timeIntervalSince(feed[indexPath.row].time!.addingTimeInterval(-60)) / 60) >= 0 {
                cell.tintColor = UIColor.gray
                if feed[indexPath.row].isDone == true {
                    cell.tintColor = UIColor.init(red: 40/255, green: 125/255, blue: 246/255, alpha: 1)
                }
                cell.accessoryType = .checkmark
            }
            return cell
        } else {
            if indexPath.row == tvFeed.numberOfRows(inSection: 0) - 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! TVCNotificationCell
                if ForCat.index(feed: feed, ind: petIndex).count == 0 {
                    cell.switch.setOn(false, animated: true)
                    cell.switch.isEnabled = false
                    cell.textLabel?.textColor = UIColor.darkGray
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(cell.switch.isOn, forKey: "notification")
                    userDefaults.synchronize()
                } else {
                    cell.switch.isEnabled = true
                    let userDefaults = UserDefaults.standard
                    if userDefaults.bool(forKey: "notification") {
                        cell.switch.setOn(true, animated: true)
                    }
                    cell.textLabel?.textColor = UIColor.black
                }
                if cell.switch.isOn {
                    center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                        if !granted {
                        }
                    }
                    center.getNotificationSettings { (settings) in
                        if settings.authorizationStatus != .authorized {
                        }
                    }
                    
                    for item in ForCat.index(feed: feed, ind: petIndex) {
                        let content = UNMutableNotificationContent()
                        content.title = "Пора кормить \(cat[petIndex].name!)!"
                        content.body = "\(item.count!) \(item.feed!)"
                        content.sound = UNNotificationSound.default
                        content.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber
                        let triggerDaily = Calendar.current.dateComponents([.hour, .minute], from: item.time!)
                        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
                        let identifier = "\(petIndex)\(item.time!)"
                        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                        center.add(request, withCompletionHandler: { (error) in
                            if error != nil {}
                        })
                    }
                }
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ZooCell")!
                cell.tintColor = UIColor.init(red: 40/255, green: 125/255, blue: 246/255, alpha: 1)
                cell.accessoryType = .disclosureIndicator
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < ForCat.index(feed: feed, ind: petIndex).count {
            if Float(Date.init().timeIntervalSince(feed[indexPath.row].time!) / 60) >= 0 {
                feed[indexPath.row].isDone = !feed[indexPath.row].isDone
                tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row >= ForCat.index(feed: feed, ind: petIndex).count {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if ForCat.index(feed: feed, ind: petIndex).count != 0 {
            if editingStyle == .delete {
                
                let alertController = UIAlertController(title: nil, message: "Удалить запись?", preferredStyle: .alert)
                let deleteAction = UIAlertAction(title: "Удалить", style: .default) { (action) in
                    self.context.delete(ForCat.index(feed: self.feed, ind: petIndex)[indexPath.row])
                    do {
                        try self.context.save()
                    } catch {
                        print(error.localizedDescription)
                    }
                    tableView.reloadData()
                }
                let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
                
                alertController.addAction(deleteAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        tabBarController?.tabBar.items?[1].badgeValue = nil
        tvFeed.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.navBar.isTranslucent = true
        self.navBar.backgroundColor = UIColor.clear
    }
}
