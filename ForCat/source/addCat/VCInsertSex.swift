//
//  VCInsertSex.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 24.07.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit

class VCInsertSex: UIViewController, UITextFieldDelegate {

    let bufSex = ["Мужской", "Женский"]
    var name: String = ""
    var photo: UIImage!
    var breed: String = ""
    var bd: Date = Date.init()
    var sex: String = ""
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {
        
    }
    
    @IBOutlet weak var tfInsertSex: UITextField!
    
    
    func choiseSex()
    {
        tfInsertSex.text = bufSex[0]
        
        let elementPicker = UIPickerView()
        elementPicker.delegate = self
        elementPicker.dataSource = self
        
        tfInsertSex.inputView = elementPicker
        tfInsertSex.inputAccessoryView = createToolBar()
        tfInsertSex.tintColor = .clear
    }
    
    func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                       target: self,
                                       action: nil)
        
        let doneButton = UIBarButtonItem(title: "Далее",
                                         style: .plain,
                                         target: self,
                                         action: #selector(dismissKeyboard))
        
        
        toolBar.setItems([flexible, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromSex" {
            let dvc = segue.destination as! VCInsertWeight
            dvc.name = name
            dvc.photo = photo
            dvc.breed = breed
            dvc.bd = bd
            dvc.sex = sex
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sex = bufSex[0]
        hideKeyboard()
        choiseSex()
        // Do any additional setup after loading the view.
    }
}

extension VCInsertSex: UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bufSex.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return bufSex[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        sex = bufSex[row]
        tfInsertSex.text = bufSex[row]
    }
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
