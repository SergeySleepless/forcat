//
//  VCPets.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 27.07.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

class VCPets: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var tvPets: UITableView!
    var cat: [Cats] = []
    
    @IBAction func bAddPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Добавить питомца?",
                                                message: nil,
                                                preferredStyle: .actionSheet)
        let addAction = UIAlertAction(title: "Добавить", style: .default) { (action) in
            let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "addCat")
            self.show(nextViewController, sender: self)
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<Cats> = Cats.fetchRequest()
        do {
            cat = try context.fetch(fetchRequest)
        } catch {
        }
        
        if cat.count == 0 {
            let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "Welcome")
            self.show(nextViewController, sender: self)
        }
        
        return cat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPet") as! TVCPet
        
        cell.ivPhoto.image = UIImage(data: cat[indexPath.row].photo!)
        cell.ivPhoto.layer.borderWidth = 1
        cell.ivPhoto.layer.masksToBounds = false
        cell.ivPhoto.layer.borderColor = UIColor.clear.cgColor
        cell.ivPhoto.layer.cornerRadius = cell.ivPhoto.frame.height/2
        cell.ivPhoto.clipsToBounds = true

        cell.lName.text = cat[indexPath.row].name
        cell.lBreed.text = cat[indexPath.row].breed
        cell.accessoryType = .none
        if indexPath.row == petIndex {
            cell.accessoryType = .checkmark
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            let alertController = UIAlertController(title: nil, message: "Удалить \(cat[indexPath.row].name!)?", preferredStyle: .alert)
            
            let deleteAction = UIAlertAction(title: "Удалить", style: .default) { (action) in
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let context = appDelegate.persistentContainer.viewContext
                context.delete(self.cat[indexPath.row])
                
                let _: NSFetchRequest<Cats> = Cats.fetchRequest()
                do {
                    try context.save()
                } catch {
                }
                
                var feedBuf: [Feed] = []
                let fetchRequest: NSFetchRequest<Feed> = Feed.fetchRequest()
                do {
                    feedBuf = try context.fetch(fetchRequest)
                } catch {
                }
                
                if feedBuf.count == 1 {
                    context.delete(feedBuf[0])
                    feedBuf.remove(at: 0)
                }
                
                let center = UNUserNotificationCenter.current()
                for item in ForCat.index(feed: feedBuf, ind: petIndex) {
                    center.removePendingNotificationRequests(withIdentifiers: ["\(indexPath.row)\(item.time!)"])
                }
                
                for item in feedBuf {
                    if item.petIndex == indexPath.row {
                        context.delete(item)
                        let index = feedBuf.index(of: item)
                        feedBuf.remove(at: index!)
                    }
                }
                
                if indexPath.row < self.cat.count {
                    for (index, item) in feedBuf.enumerated() {
                        if item.petIndex > indexPath.row {
                            feedBuf[index].petIndex -= 1
                        }
                    }
                }
                
                do {
                    try context.save()
                } catch {
                    print(error.localizedDescription)
                }
                
                tableView.reloadData()
            }
            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
            
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
            
            let userDefaults = UserDefaults.standard
            petIndex -= 1
            userDefaults.set(petIndex, forKey: "indPet")
            userDefaults.synchronize()
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        petIndex = indexPath.row
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(petIndex, forKey: "indPet")
        userDefaults.synchronize()
        
        tableView.reloadData()
        tabBarController?.tabBar.items?[0].title = cat[petIndex].name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tvPets.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.navBar.isTranslucent = true
        self.navBar.backgroundColor = UIColor.clear
    }
}
