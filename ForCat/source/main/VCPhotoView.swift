//
//  VCPhotoView.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 02.09.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import CoreData

class VCPhotoView: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var isHidden:Bool = false
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation{
        return .fade
    }
    override var prefersStatusBarHidden: Bool{
        return isHidden
    }
    @IBOutlet weak var hideBars: UIView!
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var ivPhoto: UIImageView!
    @IBAction func btMore(_ sender: UIBarButtonItem) {
    }
    @IBAction func btTap(_ sender: UIButton) {
        isHidden = !isHidden
        
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
            self.navBar.isHidden = self.isHidden
        })
    }
    
    var isCamera = false
    var activityIN : UIActivityIndicatorView = UIActivityIndicatorView()
    @IBAction func btMorePressed(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Изменить фотографию",
                                                message: nil,
                                                preferredStyle: .actionSheet)

        let cameraAction = UIAlertAction(title: "Камера", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.activityIN.startAnimating()
                self.activityIN.center = self.view.center
                self.activityIN.hidesWhenStopped = true
                self.activityIN.style = UIActivityIndicatorView.Style.gray
                self.view.addSubview(self.activityIN)

                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = true
                imagePicker.delegate = self
                
                self.isCamera = true

                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
                imagePicker.viewDidLoad()
            }
        }
        let photoLibAction = UIAlertAction(title: "Альбом", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.activityIN.startAnimating()
                self.activityIN.center = self.view.center
                self.activityIN.hidesWhenStopped = true
                self.activityIN.style = UIActivityIndicatorView.Style.gray
                self.view.addSubview(self.activityIN)

                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = true
                imagePicker.delegate = self

                imagePicker.sourceType = .photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
                imagePicker.viewDidLoad()
            }
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)

        alertController.addAction(cameraAction)
        alertController.addAction(photoLibAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        ivPhoto.image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        
        if isCamera {
            UIImageWriteToSavedPhotosAlbum(ivPhoto.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        var objects = cat
        do {
            objects = try context.fetch(Cats.fetchRequest())
        }
        catch {}
        
        let object = objects[petIndex] as NSManagedObject
        object.setValue(ivPhoto.image?.pngData(), forKey: "photo")
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
        
        ivPhoto.contentMode = .scaleAspectFill
        ivPhoto.clipsToBounds = true
        
        activityIN.stopAnimating()
        
        self.dismiss(animated: true, completion: { () -> Void in})
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Ошибка сохранения.", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Сохранено!", message: "Фото было добавлено в ваш альбом.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        activityIN.stopAnimating()
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ivPhoto.image = UIImage(data: cat[petIndex].photo!)
    }
}
