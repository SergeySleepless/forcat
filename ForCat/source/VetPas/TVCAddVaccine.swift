//
//  TVCAddVaccine.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 05/09/2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import CoreData

class TVCAddVaccine: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var tfNameVaccine: UITextField!
    @IBOutlet weak var tfVaccineOn: UITextField!
    @IBOutlet weak var tfVaccineOff: UITextField!
    var dateBuffer = String()
    
    override func awakeFromNib() {
        tfNameVaccine.inputAccessoryView = createToolBar()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale.init(identifier: "ru")
        dateBuffer = dateFormatter.string(from: Date.init())
        
        let datePicker = UIDatePicker()
        datePicker.locale = Locale.init(identifier: "ru")
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(dateChanged(sender:)), for: UIControl.Event.valueChanged)
        
        tfVaccineOff.inputView = datePicker
        tfVaccineOff.inputAccessoryView = createToolBar()
        tfVaccineOff.tintColor = .clear
        tfVaccineOff.delegate = self
        
        datePicker.maximumDate = Date.init()
        tfVaccineOn.inputView = datePicker
        tfVaccineOn.inputAccessoryView = createToolBar()
        tfVaccineOn.tintColor = .clear
        tfVaccineOn.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfVaccineOn {
            tfVaccineOn.text = dateBuffer
        }
        if textField == tfVaccineOff {
            tfVaccineOff.text = dateBuffer
        }
    }
    
    @objc func dateChanged(sender: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale.init(identifier: "ru")
        dateBuffer = dateFormatter.string(from: sender.date)
    }
    
    func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                       target: self,
                                       action: nil)
        
        let doneButton = UIBarButtonItem(title: "Готово",
                                         style: .plain,
                                         target: self,
                                         action: #selector(VCInsertName.dismissKeyboard))
        
        toolBar.setItems([flexible, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func btAddVaccinePressed(_ sender: UIButton) {
        
        if tfNameVaccine.text == "" || tfVaccineOn.text == "" || tfVaccineOff.text == "" {
            errorAlert()
            return
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        var objects = cat
        do {
            objects = try context.fetch(Cats.fetchRequest())
        }
        catch {}
        
        let object = objects[petIndex] as NSManagedObject
        
        var namesBuffer = cat[petIndex].vaccinesNames
        namesBuffer?.append(tfNameVaccine.text!)
        var vaccineOnBuffer = cat[petIndex].vaccinesOn
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale.init(identifier: "ru")
        vaccineOnBuffer?.append(dateFormatter.date(from: tfVaccineOn.text!)!)
        var vaccineOffBuffer = cat[petIndex].vaccinesOff
        vaccineOffBuffer?.append(dateFormatter.date(from: tfVaccineOff.text!)!)
        
        object.setValue(tfNameVaccine.text, forKey: "vaccinesNames")
        object.setValue(tfVaccineOn.text, forKey: "vaccinesOn")
        object.setValue(tfVaccineOff.text, forKey: "vaccinesOff")
    }
    
    // Вызывает ошибку
    func errorAlert() {
        let alert = UIAlertController(title: "Ошибка!", message: "Заполните все поля для добавления вакцины.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }}))
        //self.view.present(alert, animated: true, completion: nil)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
extension TVCAddVaccine {
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        self.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
}
