//
//  VCInsertWeight.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 24.07.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import CoreData

class VCInsertWeight: UIViewController {
    var name: String = ""
    var photo: UIImage!
    var breed: String = ""
    var bd: Date = Date.init()
    var sex: String = ""
    var weight = Float()
    @IBOutlet weak var slWeight: UISlider!
    @IBOutlet weak var lWeight: UILabel!
    @IBOutlet weak var lMinWeight: UILabel!
    @IBOutlet weak var lMaxWeight: UILabel!
    
    @IBAction func swAddWeight(_ sender: UISwitch) {
        if sender.isOn {
            slWeight.isEnabled = false
            weight = 0
            lWeight.isHidden = true
        } else {
            slWeight.isEnabled = true
            weight = slWeight.value
            lWeight.text = "\(weight) кг"
            lWeight.isHidden = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        slWeight.isEnabled = false
        initWeight()
    }
    
    @IBAction func slWeightCahged(_ sender: UISlider) {
        sender.value = sender.value.roundTo(places: 1)
        lWeight.text = "\(sender.value) кг"
        weight = sender.value
        
        delayWithSeconds(0.5) {
            if self.slWeight.maximumValue == sender.value && self.slWeight.maximumValue < 20 {
                self.slWeight.maximumValue += 1.0
                self.lMaxWeight.text = "\(self.slWeight.maximumValue)"
            }
        }
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    @IBAction func bAccept(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Cats", in: context)
        let catObject = NSManagedObject(entity: entity!, insertInto: context) as! Cats
        catObject.name = name
        catObject.photo = photo.pngData()
        catObject.breed = breed
        catObject.bd = bd
        catObject.sex = sex
        catObject.weight = weight
        
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
        let userDefaults = UserDefaults.standard
        petIndex += 1
        userDefaults.set(petIndex, forKey: "indPet")
        userDefaults.synchronize()
    }
    
    func initWeight() {
        
        let calendar = Calendar.current
        let comp = calendar.dateComponents([.month], from: bd, to: Date.init())
        if comp.month! <= 6 {
            slWeight.value = (catsWeightKitty[comp.month!].maxCritical + catsWeightKitty[comp.month!].minCritical) / 2
            slWeight.minimumValue = catsWeightKitty[comp.month!].minCritical
            slWeight.maximumValue = catsWeightKitty[comp.month!].maxCritical
        } else {
            for item in CatsBreedsWeight {
                if item.breed == breed && item.sex == sex {
                    slWeight.minimumValue = item.minAverage - 2.0
                    if slWeight.minimumValue <= 0 {
                        slWeight.minimumValue = 1.0
                    }
                    slWeight.maximumValue = item.maxAverage + 2.0
                }
            }
        }
        lMinWeight.text = "\(slWeight.minimumValue)"
        lMaxWeight.text = "\(slWeight.maximumValue)"
        slWeight.value = (slWeight.maximumValue + slWeight.minimumValue) / 2
        lWeight.text = "\(slWeight.value) кг"
        lWeight.isHidden = true
    }
}
extension Float {
    func roundTo(places: Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}
