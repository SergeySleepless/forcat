//
//  VCAddFeed.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 29.07.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import CoreData

class VCAddFeed: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var dpTime: UIDatePicker!
    @IBOutlet weak var tfFeed: UITextField!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var tfFeedCount: UITextField!
    
    func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                       target: self,
                                       action: nil)
        
        let doneButton = UIBarButtonItem(title: "Готово",
                                         style: .plain,
                                         target: self,
                                         action: #selector(VCInsertName.dismissKeyboard))
        
        
        toolBar.setItems([flexible, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.navBar.isTranslucent = true
        self.navBar.backgroundColor = UIColor.clear
        
        tfFeed.autocapitalizationType = .sentences
        tfFeed.delegate = self
        tfFeed.inputAccessoryView = createToolBar()
        
        tfFeedCount.autocapitalizationType = .sentences
        tfFeedCount.delegate = self
        tfFeedCount.inputAccessoryView = createToolBar()
        
        dpTime.date = Date.init().addingTimeInterval(5)
        
        hideKeyboard()
    }
    
    @IBAction func btAdd(_ sender: UIBarButtonItem) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        var feedBuf: [Feed] = []
        let fetchRequest: NSFetchRequest<Feed> = Feed.fetchRequest()
        do {
            feedBuf = try context.fetch(fetchRequest)
        } catch {
        }
        
        var ind = true
        for item in feedBuf {
            if item.petIndex == petIndex {
                if Int(item.time!.timeIntervalSince(dpTime.date) / 60) == 0 {
                    if item.feed == tfFeed.text && item.count == tfFeedCount.text {
                        ind = false
                        break
                    }
                }
            }
        }
        
        if ind == true {
            let entity = NSEntityDescription.entity(forEntityName: "Feed", in: context)
            let feedObject = NSManagedObject(entity: entity!, insertInto: context) as! Feed
            feedObject.petIndex = Int64(petIndex)
            feedObject.time = dpTime.date
            feedObject.feed = tfFeed.text
            feedObject.count = tfFeedCount.text
            do {
                try context.save()
            } catch {
                print(error.localizedDescription)
            }
        }
        
        self.performSegue(withIdentifier: "fromFeedToAddFeed", sender: self)
    }
}
extension VCAddFeed {
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

