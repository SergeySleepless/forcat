//
//  VCInsertBD.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 24.07.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit

class VCInsertBD: UIViewController, UITextFieldDelegate {
    
    var name: String = ""
    var photo: UIImage!
    var breed: String = ""
    var bd: Date = Date.init()
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {
        
    }
    
    @IBOutlet weak var tfInsertBD: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboard()
        choiseBD()
    }
    
    func choiseBD()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale.init(identifier: "ru")
        
        tfInsertBD.text = dateFormatter.string(from: Date.init())
        
        let datePicker = UIDatePicker()
        datePicker.locale = Locale.init(identifier: "ru")
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date.init()
        datePicker.addTarget(self, action: #selector(dateChanged(sender:)), for: UIControl.Event.valueChanged)
        
        tfInsertBD.inputView = datePicker
        tfInsertBD.inputAccessoryView = createToolBar()
        tfInsertBD.tintColor = .clear
    }
    
    @objc func dateChanged(sender: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale.init(identifier: "ru")
        tfInsertBD.text = dateFormatter.string(from: sender.date)
        bd = sender.date
    }
    
    func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                       target: self,
                                       action: nil)
        
        let doneButton = UIBarButtonItem(title: "Далее",
                                         style: .plain,
                                         target: self,
                                         action: #selector(VCInsertName.dismissKeyboard))
        
        
        toolBar.setItems([flexible, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromBD" {
            let dvc = segue.destination as! VCInsertSex
            dvc.name = name
            dvc.photo = photo
            dvc.breed = breed
            dvc.bd = bd
        }
    }
}

extension VCInsertBD {
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
