//
//  VCMain.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 22.07.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import CoreData

var cat: [Cats] = []

class VCProfile: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var barName: UITabBarItem!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var viewEditWeight: UIView!
    @IBOutlet weak var slWeight: UISlider!
    @IBOutlet weak var slNewWeight: UISlider!
    @IBOutlet weak var lNewWeight: UILabel!
    @IBOutlet weak var lMinWeight: UILabel!
    @IBOutlet weak var lMaxWeight: UILabel!
    @IBOutlet weak var lNewMinWeight: UILabel!
    @IBOutlet weak var lNewMaxWeight: UILabel!
    @IBOutlet weak var imBackground: UIImageView!
    @IBOutlet weak var btClose: UIButton!
    @IBOutlet weak var btClose2: UIButton!
    @IBOutlet weak var vNewWeight: UIView!
    @IBOutlet weak var viewPhoto: UIView!
    @IBAction func btViewPhotoPressed(_ sender: UIButton) {
        viewPhoto.isHidden = false
        self.tabBarController!.tabBar.isHidden = true
    }
    
    @IBAction func slWeightEdit(_ sender: UISlider) {
        sender.value = sender.value.roundTo(places: 1)
        lNewWeight.text = "\(sender.value) кг"
    }
    
    @IBAction func btClose(_ sender: UIButton) {
        setView(view: viewEditWeight, hidden: true)
        btClose.isHidden = true
        btClose2.isHidden = true
    }
    @IBAction func btClose2(_ sender: UIButton) {
        setView(view: viewEditWeight, hidden: true)
        btClose.isHidden = true
        btClose2.isHidden = true
    }
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {}
    
    @IBAction func bAcceptNewWeight(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        var objects = cat
        do {
            objects = try context.fetch(Cats.fetchRequest())
        }
        catch {}
        
        let object = objects[petIndex] as NSManagedObject
        object.setValue(slNewWeight.value, forKey: "weight")
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
        cat[petIndex].weight = slNewWeight.value
        
        weight.text = "Вес: \(slNewWeight.value) кг"
        slWeight.isHidden = false
        slWeight.value = slNewWeight.value
        setView(view: viewEditWeight, hidden: true)
        btClose.isHidden = true
        btClose2.isHidden = true
        
        if slNewWeight.value > 0 {
            weight.text = "Вес: \(slNewWeight.value) кг"
            slWeight.value = slNewWeight.value
            slWeight.isHidden = false
            lMinWeight.isHidden = false
            lMaxWeight.isHidden = false
        } else {
            weight.text = "Вес: неизвестно"
            slWeight.isHidden = true
            lMinWeight.isHidden = true
            lMaxWeight.isHidden = true
        }
    }
    @IBAction func bCancelNewWeight(_ sender: UIButton) {
        setView(view: viewEditWeight, hidden: true)
        btClose.isHidden = true
        btClose2.isHidden = true
    }
    
    @IBAction func btEditWeight(_ sender: UIButton) {
        slNewWeight.value = cat[petIndex].weight
        lNewWeight.text = "\(cat[petIndex].weight) кг"
        slNewWeight.isEnabled = true
        setView(view: viewEditWeight, hidden: false)
        btClose.isHidden = false
        btClose2.isHidden = false
    }
    
    func viewCat() {
        name.text = "Имя: \((cat[petIndex].name)!)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.locale = Locale.init(identifier: "ru")
        age.text = "Возраст: "
        let comp = Calendar.current.dateComponents([.month, .year], from: cat[petIndex].bd!, to: Date.init())
        if comp.year! % 10 < 5 && comp.year! > 0 {
            age.text?.append("\(comp.year!) года")
        } else if comp.year! % 10 >= 5{
            age.text?.append("\(comp.year!) лет")
        }
        if comp.month! < 5 && comp.month! > 0 {
            if (comp.year! != 0) {
                age.text?.append(", \(comp.month!) месяца")
            } else {
                age.text?.append("\(comp.month!) месяца")
            }

        } else if comp.month! >= 5 {
            if (comp.year! != 0) {
                age.text?.append(", \(comp.month!) месяцев")
            } else {
                age.text?.append("\(comp.month!) месяцев")
            }
        }
        if comp.year! < 1 && comp.month! < 1
        {
            age.text?.append("меньше месяца")
        }
        if cat[petIndex].weight > 0 {
            weight.text = "Вес: \(cat[petIndex].weight) кг"
            slWeight.value = cat[petIndex].weight
            slWeight.isHidden = false
            lMinWeight.isHidden = false
            lMaxWeight.isHidden = false
        } else {
            weight.text = "Вес: неизвестно"
            slWeight.isHidden = true
            lMinWeight.isHidden = true
            lMaxWeight.isHidden = true
        }
        slWeight.isEnabled = false
        photo.image = UIImage(data: cat[petIndex].photo!)
        photo.layer.borderWidth = 1
        photo.layer.masksToBounds = false
        photo.layer.borderColor = UIColor.clear.cgColor
        photo.layer.cornerRadius = photo.frame.height/2
        photo.clipsToBounds = true

        barName.title = cat[petIndex].name
        initWeight()
    }
    override func viewWillAppear(_ animated: Bool) {
        if UIApplication.shared.applicationIconBadgeNumber != 0 {
            tabBarController?.tabBar.items?[1].badgeValue = "\(UIApplication.shared.applicationIconBadgeNumber)"
        } else {
            tabBarController?.tabBar.items?[1].badgeValue = nil
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<Cats> = Cats.fetchRequest()
        
        do {
            cat = try context.fetch(fetchRequest)
        } catch {
        }
        viewCat()
        viewEditWeight.isHidden = true
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        vNewWeight.layer.cornerRadius = 13
        vNewWeight.clipsToBounds = true
    }
    
    func initWeight() {
        let calendar = Calendar.current
        let comp = calendar.dateComponents([.month], from: cat[petIndex].bd!, to: Date.init())
        if comp.month! <= 6 {
            slNewWeight.value = (catsWeightKitty[comp.month!].maxCritical + catsWeightKitty[comp.month!].minCritical) / 2
            slNewWeight.minimumValue = catsWeightKitty[comp.month!].minCritical
            slNewWeight.maximumValue = catsWeightKitty[comp.month!].maxCritical
            slWeight.minimumValue = catsWeightKitty[comp.month!].minCritical
            slWeight.maximumValue = catsWeightKitty[comp.month!].maxCritical

        } else {
            for item in CatsBreedsWeight {
                if item.breed == cat[petIndex].breed && item.sex == cat[petIndex].sex {
                    slNewWeight.minimumValue = item.minAverage - 2.0
                    slWeight.minimumValue = item.minAverage - 2.0
                    if slNewWeight.minimumValue <= 0 {
                        slNewWeight.minimumValue = 1.0
                    }
                    if slWeight.minimumValue <= 0 {
                        slWeight.minimumValue = 1.0
                    }
                    slNewWeight.maximumValue = item.maxAverage + 2.0
                    slWeight.maximumValue = item.maxAverage + 2.0
                }
            }
        }
        lNewMinWeight.text = "\(slNewWeight.minimumValue)"
        lNewMaxWeight.text = "\(slNewWeight.maximumValue)"
        slNewWeight.value = (slNewWeight.maximumValue + slWeight.minimumValue) / 2
        lNewWeight.text = "\(slNewWeight.value) кг"
        
        lMinWeight.text = "\(slNewWeight.minimumValue)"
        lMaxWeight.text = "\(slNewWeight.maximumValue)"
    }
}
