//
//  insertPhoto.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 24.07.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit

class VCinsertPhoto: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var bNext: UIButton!
    var name: String = ""
    @IBOutlet weak var ivPhoto: UIImageView!
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bNext.setTitle("Пропустить", for: .normal)
    }
    
    // колесо загрузки
    var activityIN : UIActivityIndicatorView = UIActivityIndicatorView()
    
    // Когда жанимаем на фото
    @IBAction func bChangePhoto(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: nil,
                                                message: nil,
                                                preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Камера", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.activityIN.startAnimating()
                self.activityIN.center = self.view.center
                self.activityIN.hidesWhenStopped = true
                self.activityIN.style = UIActivityIndicatorView.Style.gray
                
                self.view.addSubview(self.activityIN)
                
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = true
                imagePicker.delegate = self
                
                
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
                imagePicker.viewDidLoad()
            }
        }
        
        let photoLibAction = UIAlertAction(title: "Фото", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.activityIN.startAnimating()
                self.activityIN.center = self.view.center
                self.activityIN.hidesWhenStopped = true
                self.activityIN.style = UIActivityIndicatorView.Style.gray
                self.view.addSubview(self.activityIN)
                
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = true
                imagePicker.delegate = self
                
                imagePicker.sourceType = .photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
                imagePicker.viewDidLoad()
            }
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        ivPhoto.image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        ivPhoto.contentMode = .scaleAspectFill
        ivPhoto.clipsToBounds = true
        
        activityIN.stopAnimating()
        bNext.setTitle("Далее", for: .normal)
        
        self.dismiss(animated: true, completion: { () -> Void in})
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        activityIN.stopAnimating()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromPhoto" {
            let dvc = segue.destination as! VCInsertBreed
            dvc.name = name
            dvc.photo = ivPhoto.image
        }
    }
}
