//
//  InsertBreed.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 24.07.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit

class VCInsertBreed: UIViewController, UITextFieldDelegate {

    var name: String = ""
    var photo: UIImage!
    var breed: String = ""
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {
        
    }
    
    @IBOutlet weak var tfInsertBreed: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        breed = breeds[0]
        
        hideKeyboard()
        choiseBreed()
    }
    
    func choiseBreed()
    {
        tfInsertBreed.text = breeds[0]
        
        let elementPicker = UIPickerView()
        elementPicker.delegate = self
        elementPicker.dataSource = self
        
        tfInsertBreed.inputView = elementPicker
        tfInsertBreed.inputAccessoryView = createToolBar()
        tfInsertBreed.tintColor = .clear
    }
    
    func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                       target: self,
                                       action: nil)
        
        let doneButton = UIBarButtonItem(title: "Далее",
                                         style: .plain,
                                         target: self,
                                         action: #selector(dismissKeyboard))
        
        
        toolBar.setItems([flexible, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromBreed" {
            let dvc = segue.destination as! VCInsertBD
            dvc.name = name
            dvc.photo = photo
            dvc.breed = breed
        }
    }
}

extension VCInsertBreed: UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return breeds.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return breeds[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        breed = breeds[row]
        tfInsertBreed.text = breeds[row]
    }
    
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
