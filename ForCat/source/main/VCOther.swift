//
//  VCOther.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 08.08.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import MapKit

class VCOther: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    @IBOutlet weak var tvOther: UITableView!
    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {}
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell")!
        if indexPath.row == 0 {
            cell.textLabel?.text = "Ветеринарный паспорт"
        }
        if indexPath.row == 1 {
            cell.textLabel?.text = "Ветеринарные клиники рядом"
        }
        cell.tintColor = UIColor.red
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            
            let locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            
            // Check for Location Services
            if (CLLocationManager.locationServicesEnabled()) {
                locationManager.requestAlwaysAuthorization()
                locationManager.requestWhenInUseAuthorization()
            }
            
            let mapView = MKMapView()
            
            let request = MKLocalSearch.Request()
            request.naturalLanguageQuery = "Pizza"
            request.region = mapView.region
            
            let search = MKLocalSearch(request: request)
            
            search.start(completionHandler: {(response, error) in
                
                if error != nil {
                    print("Error occurred in search: \(error!.localizedDescription)")
                } else if response!.mapItems.count == 0 {
                    print("No matches found")
                } else {
                    print("Matches found")
                    
                    for item in response!.mapItems {
                        print("Name = \(item.name)")
                        print("Phone = \(item.phoneNumber)")
                    }
                }
            })
            
//            var vetsMapItem: [MKMapItem] = []
//            let request = MKLocalSearch.Request()
//            request.region = MKCoordinateRegionMakeWithDistance(viewRegion, 200, 200)
//            request.naturalLanguageQuery = "Ветеринарные клиники"
//            let search = MKLocalSearch(request: request)
//            MKMapItem.openMaps(with: vetsMapItem, launchOptions: nil)
            //mapItem.openInMaps(launchOptions: )
        }
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "vetPas")
            present(vc!, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.navBar.isTranslucent = true
        self.navBar.backgroundColor = UIColor.clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tvOther.reloadData()
    }
}
