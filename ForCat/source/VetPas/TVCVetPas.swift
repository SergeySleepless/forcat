//
//  TVCVetPas.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 30.08.2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import CoreData
import NotificationCenter

class TVCVetPas: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate  {
    @IBOutlet var collectionOfLabels: Array<UILabel>?
    @IBOutlet var collectionOfTextFields: Array<UITextField>?
    @IBOutlet weak var btEdit: UIBarButtonItem!
    @IBOutlet weak var btBack: UIBarButtonItem!
    @IBOutlet weak var ivPhoto: UIImageView!
    @IBOutlet weak var btEditPhoto: UIButton!
    var tfBuffer: [String] = []
    var animatedTextfields: Bool = false
    var ivBuffer = UIImage()
    // variable for textFields
    var breed: String = ""
    @IBOutlet weak var tvVetPas: UITableView!
    
    @IBAction func btEditPressed(_ sender: UIBarButtonItem) {
        switch sender.title! {
        case "Изменить":
            for i in 0..<collectionOfLabels!.count {
                collectionOfTextFields![i].font = UIFont(name: collectionOfLabels![i].font.familyName, size: 30)
                collectionOfTextFields![i].font = collectionOfLabels![i].font
                collectionOfTextFields![i].textAlignment = collectionOfLabels![i].textAlignment
                collectionOfTextFields![i].text = collectionOfLabels![i].text
                collectionOfTextFields![i].isHidden = false
                collectionOfTextFields![i].shake()
                collectionOfLabels![i].isHidden = true
                print(i)
            }
            ivPhoto.shake()
            animatedTextfields = true
            btEditPhoto.isHidden = false
            btEdit.title = "Сохранить"
            btBack.title = "Отменить"
            break
        case "Сохранить":
            for i in 0..<collectionOfLabels!.count {
                collectionOfLabels![i].text = collectionOfTextFields![i].text
                collectionOfLabels![i].isHidden = false
                collectionOfTextFields![i].isHidden = true
            }
            ivPhoto.stopShake()
            animatedTextfields = false
            btEditPhoto.isHidden = true
            btEdit.title = "Изменить"
            btBack.title = "Назад"
            dismissKeyboard()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            var objects = cat
            do {
                objects = try context.fetch(Cats.fetchRequest())
            }
            catch {}
            
            let object = objects[petIndex] as NSManagedObject
            object.setValue(collectionOfLabels![0].text, forKey: "ownerOneFirstName")
            object.setValue(collectionOfLabels![1].text, forKey: "ownerOneLastName")
            object.setValue(collectionOfLabels![2].text, forKey: "ownerOneAddress")
            object.setValue(collectionOfLabels![3].text, forKey: "name")
            object.setValue(collectionOfLabels![4].text, forKey: "type")
            object.setValue(collectionOfLabels![5].text, forKey: "breed")
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            dateFormatter.locale = Locale.init(identifier: "ru")
            object.setValue(dateFormatter.date(from: collectionOfLabels![6].text!), forKey: "bd")
            object.setValue(collectionOfLabels![7].text, forKey: "skin")
            object.setValue(collectionOfLabels![8].text, forKey: "chipNumber")
            object.setValue(dateFormatter.date(from: collectionOfLabels![9].text!), forKey: "chipDate")
            object.setValue(collectionOfLabels![10].text, forKey: "chipPlacement")
            object.setValue(collectionOfLabels![11].text, forKey: "stigmaNumber")
            object.setValue(dateFormatter.date(from: collectionOfLabels![12].text!), forKey: "stigmaDate")
            object.setValue(collectionOfLabels![13].text, forKey: "signs")
            object.setValue(collectionOfLabels![14].text, forKey: "reproduction")
            do {
                try context.save()
            } catch {
                print(error.localizedDescription)
            }
            break
        default: break
        }
        tvVetPas.beginUpdates()
        tvVetPas.endUpdates()
    }
    
    @IBAction func btBackPressed(_ sender: UIBarButtonItem) {
        switch sender.title {
        case "Назад":
            self.dismiss(animated: true, completion: nil)
            dismissKeyboard()
        case "Отменить":
            for i in 0..<collectionOfLabels!.count {
                collectionOfLabels![i].text = collectionOfTextFields![i].text
                collectionOfLabels![i].isHidden = false
                collectionOfTextFields![i].isHidden = true
            }
            btEditPhoto.isHidden = true
            ivPhoto.stopShake()
            animatedTextfields = false
            dismissKeyboard()
            btEdit.title = "Изменить"
            btBack.title = "Назад"
            break
        default: break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = false
        hideKeyboard()
        for i in 0..<collectionOfTextFields!.count {
            collectionOfTextFields![i].delegate = self
            //collectionOfTextFields![i].clearButtonMode = .unlessEditing
        }
        ivPhoto.image = UIImage(data: cat[petIndex].photo!)
        ivBuffer = ivPhoto.image!
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        collectionOfTextFields![(collectionOfTextFields?.index(of: textField)!)!].stopShake()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let numberOfTextField = collectionOfTextFields?.index(of: textField)!
        if animatedTextfields {
            collectionOfTextFields![numberOfTextField!].shake()
        }
        if collectionOfTextFields!.startIndex.distance(to: numberOfTextField!) < (collectionOfLabels?.count)! {
            tfBuffer[numberOfTextField!] = collectionOfTextFields![numberOfTextField!].text!
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if animatedTextfields {
            for i in 0..<collectionOfLabels!.count {
                collectionOfTextFields![i].font = UIFont(name: collectionOfLabels![i].font.familyName, size: 30)
                collectionOfTextFields![i].font = collectionOfLabels![i].font
                collectionOfTextFields![i].textAlignment = collectionOfLabels![i].textAlignment
                collectionOfTextFields![i].text = collectionOfLabels![i].text
                collectionOfTextFields![i].isHidden = false
                collectionOfTextFields![i].shake()
                collectionOfLabels![i].isHidden = true
            }
            for i in 0..<collectionOfTextFields!.count - 3 {
                collectionOfTextFields![i].text = tfBuffer[i]
            }
            ivPhoto.shake()
            ivPhoto.image = ivBuffer
        }
    }
    
    // Убирает клавиатуру по кнопке Return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<Cats> = Cats.fetchRequest()
        
        do {
            cat = try context.fetch(fetchRequest)
        } catch {
        }
        
        // OWNER
        // FirstName
        // ==============================-------------------------------------
        collectionOfLabels![0].text = cat[petIndex].ownerOneFirstName
        // LastName
        // ==============================-------------------------------------
        collectionOfLabels![1].text = cat[petIndex].ownerOneLastName
        // Address
        // ==============================-------------------------------------
        collectionOfLabels![2].text = cat[petIndex].ownerOneAddress
        
        // PET
        // Name
        // ==============================-------------------------------------
        collectionOfLabels![3].text = cat[petIndex].name
        // Type
        // ==============================-------------------------------------
        collectionOfLabels![4].text = cat[petIndex].type
        // Breeds
        // ==============================-------------------------------------
        collectionOfLabels![5].text = cat[petIndex].breed
        let elementPicker = UIPickerView()
        elementPicker.delegate = self
        elementPicker.dataSource = self
        collectionOfTextFields![5].inputView = elementPicker
        collectionOfTextFields![5].inputAccessoryView = createToolBar()
        collectionOfTextFields![5].tintColor = .clear
        // BD
        // ==============================-------------------------------------
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.locale = Locale.init(identifier: "ru")
        collectionOfLabels![6].text = dateFormatter.string(from: cat[petIndex].bd!)
        let datePicker = UIDatePicker()
        datePicker.locale = Locale.init(identifier: "ru")
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date.init()
        datePicker.addTarget(self, action: #selector(dateChanged(sender:)), for: UIControl.Event.valueChanged)
        collectionOfTextFields![6].inputView = datePicker
        collectionOfTextFields![6].inputAccessoryView = createToolBar()
        collectionOfTextFields![6].tintColor = .clear
        // Skin
        // ==============================-------------------------------------
        collectionOfLabels![7].text = cat[petIndex].skin
        // ChipNumber
        // ==============================-------------------------------------
        collectionOfLabels![8].text = cat[petIndex].chipNumber
        // ChipDate
        // ==============================-------------------------------------
        if cat[petIndex].chipDate != nil {
            collectionOfLabels![9].text = dateFormatter.string(from: cat[petIndex].chipDate!)
        } else {
            collectionOfLabels![9].text = ""
        }
        collectionOfTextFields![9].inputView = datePicker
        collectionOfTextFields![9].inputAccessoryView = createToolBar()
        collectionOfTextFields![9].tintColor = .clear
        // ChipPlacement
        // ==============================-------------------------------------
        collectionOfLabels![10].text = cat[petIndex].chipPlacement
        // StigmaNumber
        // ==============================-------------------------------------
        collectionOfLabels![11].text = cat[petIndex].stigmaNumber
        // StigmaDate
        // ==============================-------------------------------------
        if cat[petIndex].stigmaDate != nil {
            collectionOfLabels![12].text = dateFormatter.string(from: cat[petIndex].stigmaDate!)
        } else {
            collectionOfLabels![12].text = ""
        }
        collectionOfTextFields![12].inputView = datePicker
        collectionOfTextFields![12].inputAccessoryView = createToolBar()
        collectionOfTextFields![12].tintColor = .clear
        // Sings
        // ==============================-------------------------------------
        collectionOfLabels![13].text = cat[petIndex].signs
        // Reproduction
        // ==============================-------------------------------------
        collectionOfLabels![14].text = cat[petIndex].reproduction
        
        if animatedTextfields {
            for i in 0..<collectionOfTextFields!.count {
                collectionOfTextFields![i].shake()
            }
            ivPhoto.shake()
        } else {
            for i in 0..<collectionOfTextFields!.count {
                collectionOfTextFields![i].stopShake()
            }
            ivPhoto.stopShake()
        }
        for i in 0..<collectionOfTextFields!.count - 3 {
            collectionOfTextFields![i].delegate = self
            if collectionOfLabels![i].text != nil {
                tfBuffer.append(collectionOfLabels![i].text!)
            } else {
                tfBuffer.append("")
            }
        }
    }

    @objc func dateChanged(sender: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale.init(identifier: "ru")
        collectionOfTextFields![6].text = dateFormatter.string(from: sender.date)
    }
    
    // колесо загрузки
    var activityIN : UIActivityIndicatorView = UIActivityIndicatorView()
    
    // Когда нажимаем на фото
    @IBAction func btEditPhotoPressed(_ sender: UIButton) {
        self.ivPhoto.stopShake()
        
        let alertController = UIAlertController(title: nil,
                                                message: nil,
                                                preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Камера", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.activityIN.startAnimating()
                self.activityIN.center = self.view.center
                self.activityIN.hidesWhenStopped = true
                self.activityIN.style = UIActivityIndicatorView.Style.gray
                
                self.view.addSubview(self.activityIN)
                
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = true
                imagePicker.delegate = self
                
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
                imagePicker.viewDidLoad()
            }
        }
        
        let photoLibAction = UIAlertAction(title: "Фото", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.activityIN.startAnimating()
                self.activityIN.center = self.view.center
                self.activityIN.hidesWhenStopped = true
                self.activityIN.style = UIActivityIndicatorView.Style.gray
                self.view.addSubview(self.activityIN)
                
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = true
                imagePicker.delegate = self
                
                imagePicker.sourceType = .photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
                imagePicker.viewDidLoad()
            }
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel){ (action) in
            self.ivPhoto.shake()
        }
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        ivPhoto.image = (info[UIImagePickerController.InfoKey.editedImage] as? UIImage)!
        ivPhoto.contentMode = .scaleAspectFill
        ivPhoto.clipsToBounds = true
        ivBuffer = ivPhoto.image!
        
        activityIN.stopAnimating()
        
        self.dismiss(animated: true, completion: { () -> Void in})
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        activityIN.stopAnimating()
        self.dismiss(animated: true, completion: nil)
    }
    
    func createToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace,
                                       target: self,
                                       action: nil)
        
        let doneButton = UIBarButtonItem(title: "Готово",
                                         style: .plain,
                                         target: self,
                                         action: #selector(dismissKeyboard))
        
        
        toolBar.setItems([flexible, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension TVCVetPas {
    // Прячет клавиатуру при нажатии на View
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

public extension UIView {
    func shake(count: Float = 4, for duration: TimeInterval = 0.5, withTranslation translation: Float = -2) {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = -0.015
        rotationAnimation.toValue = 0.015
        rotationAnimation.duration = 1.0
        rotationAnimation.repeatCount = .infinity
        rotationAnimation.duration = duration/TimeInterval(3)
        rotationAnimation.autoreverses = true
        layer.add(rotationAnimation, forKey: "shake")
    }
    func stopShake() {
        layer.removeAllAnimations()
    }
}
extension TVCVetPas: UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return breeds.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return breeds[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        breed = breeds[row]
        collectionOfLabels![5].text = breeds[row]
        collectionOfTextFields![5].text = breeds[row]
    }
}
