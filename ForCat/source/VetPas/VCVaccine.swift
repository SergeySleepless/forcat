//
//  VCVaccine.swift
//  ForCat
//
//  Created by Сергей Гаврилов on 05/09/2018.
//  Copyright © 2018 Сергей Гаврилов. All rights reserved.
//

import UIKit
import CoreData

class VCVaccine: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tvVaccines: UITableView!
    @IBOutlet weak var tvcAddVaccinate: TVCAddVaccine!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cat[petIndex].vaccinesNames != nil {
            return cat[petIndex].vaccinesNames!.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard cat[petIndex].vaccinesNames?.count != nil else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellAddVaccine") as! TVCAddVaccine
            return cell
        }
        if indexPath.row != cat[petIndex].vaccinesNames?.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellAddVaccine") as! TVCAddVaccine
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellVaccine") as! TVCVaccineCellTableViewCell
            cell.lVaccineName.text = cat[petIndex].vaccinesNames![indexPath.row]
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            dateFormatter.locale = Locale.init(identifier: "ru")
            cell.lVaccineOn.text = dateFormatter.string(from: cat[petIndex].vaccinesOn![indexPath.row])
            cell.lVaccineOff.text = dateFormatter.string(from: cat[petIndex].vaccinesOff![indexPath.row])
            return cell
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()    }
}
